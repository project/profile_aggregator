<?php

/**
 * @file
 * Theme and template preprocessing code for profile aggregation.
 */

/**
 * Implementation of profile_aggregator_preprocess_HOOK().
 */
function profile_aggregator_preprocess_profile_aggregator_display_view(&$variables) {
  $element = $variables['element'];
  $variables['target_type'] = $element['#target_type'];
  $variables['uid'] = $element['#uid'];
  $variables['source_node_types'] = array();
  foreach ($element['#source_nodes'] as $source_node) {
    $variables['source_node_types'][$source_node->type] = $source_node->type;
    $variables['source_nodes'][$source_node->type] = $source_node;
    $variables['rendered_nodes'][$source_node->type] = node_view($source_node, FALSE, TRUE, FALSE);
    $source_node_built = node_build_content($source_node, FALSE, TRUE);
    foreach (element_children($source_node_built->content) as $field) {
      $variables['rendered_node_fields'][$source_node->type][$field] = drupal_render($source_node_built->content[$field]);
    }
  }
}

