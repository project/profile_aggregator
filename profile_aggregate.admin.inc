<?php

/**
 * @file
 * Profile Aggregator - Administration cruft.
 */

/**
 * Admin form builder.
 */
function profile_aggregator_form_admin_settings() {
  $node_types = node_get_types('types');
  $options = array();
  foreach ($node_types as $node_type) {
    if (!is_content_profile($node_type)) {
      continue;
    }
    $options[$node_type->type] = $node_type->name;
  }
  $form = array();
  $form['profile_aggregator_sources'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Source content profiles'),
    '#options' => $options,
    '#default_value' => variable_get('profile_aggregator_sources', array()),
  );
  $form['profile_aggregator_target'] = array(
    '#type' => 'select',
    '#title' => t('Target content profile'),
    '#options' => $options,
    '#default_value' => variable_get('profile_aggregator_target', ''),
  );
  $formats = array();
  foreach (filter_formats() as $format) {
    $formats[$format->format] = $format->name;
  }
  $form['profile_aggregation_target_format'] = array(
    '#type' => 'select',
    '#title' => t('Target content profile\'s filter format'),
    '#options' => $formats,
    '#default_value' => variable_get('profile_aggregation_target_format', 1),
  );
  return system_settings_form($form);
}

/**
 * Validator for admin form.
 */
function profile_aggregator_form_admin_settings_validate($form, &$form_state) {
  // Do not perform checks if resetting to defaults.
  if ($form_state['clicked_button']['#value'] == t('Reset to defaults')) {
    return;
  }
  if ($form_state['values']['profile_aggregator_sources'][$form_state['values']['profile_aggregator_target']]) {
    form_set_error('profile_aggregator_sources', t('Target content profile must not be selected as source as well.'));
  }
}

