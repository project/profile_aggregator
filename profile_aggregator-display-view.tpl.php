<?php

/**
 * @file profile_aggregator-display-view.tpl.php
 *
 * Theme implementation to display an aggregated content profile.
 */
foreach ($source_node_types as $source_node_type) {
  foreach ($rendered_node_fields[$source_node_type] as $field) {
    print $field . '<br />';
  }
}
